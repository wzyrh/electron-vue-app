/*
 * @Author: wuziyang 764401855@qq.com
 * @Date: 2022-06-29 14:27:30
 * @LastEditors: wuziyang 764401855@qq.com
 * @LastEditTime: 2022-06-29 14:31:26
 * @FilePath: \electron-vue-app\tailwind.config.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
/** @type {import('tailwindcss').Config} */
module.exports = {
  purge: [
    './src/**/*.html',
    './src/**/*.js',
  ],
  content: [],
  theme: {
    extend: {},
  },
  plugins: [],
}
