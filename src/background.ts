/*
 * @Author: wuziyang 764401855@qq.com
 * @Date: 2022-06-22 09:48:19
 * @LastEditors: wuziyang 764401855@qq.com
 * @LastEditTime: 2022-06-28 15:41:45
 * @FilePath: \electron-vue-app\src\background.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
'use strict'

import { app, protocol, BrowserWindow, Menu } from 'electron'
import { createProtocol } from 'vue-cli-plugin-electron-builder/lib'
import installExtension, { VUEJS3_DEVTOOLS } from 'electron-devtools-installer'
// import ElementPlus from 'element-plus'
// console.log(ElementPlus)
const isDevelopment = process.env.NODE_ENV !== 'production'
// const notification = new Notification({
//   title: '通知',
//   subtitle: '再通知',
//   body: '就不给你打开嘿嘿'
// })
try {
  require('electron-reloader')(module)
} catch (_) {}
// Scheme must be registered before the app is ready
protocol.registerSchemesAsPrivileged([
  { scheme: 'app', privileges: { secure: true, standard: true } }
])

async function createWindow() {
  // Create the browser window.
  const win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      
      // Use pluginOptions.nodeIntegration, leave this alone
      // See nklayman.github.io/vue-cli-plugin-electron-builder/guide/security.html#node-integration for more info
      nodeIntegration: process.env.ELECTRON_NODE_INTEGRATION,
      contextIsolation: !process.env.ELECTRON_NODE_INTEGRATION
    }
  })

  if (process.env.WEBPACK_DEV_SERVER_URL) {
    // Load the url of the dev server if in development mode
    await win.loadURL(process.env.WEBPACK_DEV_SERVER_URL)
    if (!process.env.IS_TEST) win.webContents.openDevTools()
  } else {
    createProtocol('app')
    // Load the index.html when not in development
    win.loadURL('app://./index.html')
  }
}

const menuTemp = [
  {
    label: '文件',
    submenu: [
      {
        label: '打开文件',
        click() {
          // notification.show()
        }
      },
      {
        type: 'separator',
      },
      {
        label: '关闭文件夹',
      },
      {
        label: '关于',
        role: 'about',
      },
    ]
  },
  { 
    label: '编辑', 
  },
  {
    label: '角色',
    submenu: [
      { label: '复制', role: 'copy' },
      { label: '剪切', role: 'cut' },
      { label: '粘贴', role: 'paste' },
      { label: '最小化', role: 'minimize' },
    ]
  },
  {
    label: '类型',
    submenu: [
      { label: '选项1', type: 'checkbox' },
      { label: '选项2', type: 'checkbox' },
      { label: '选项3', type: 'checkbox' },
      { type: "separator" },
      { label: 'item1', type: "radio" },
      { label: 'item2', type: "radio" },
      { type: "separator" },
      { label: 'windows', type: 'submenu', role: 'windowMenu' }
    ]
  },
  {
    label: '其它',
    submenu: [
      {
        label: '打开',
        accelerator: 'ctrl + o',
        click() {
          console.log('open操作执行了')
        }
      }
    ]
  },
]

const menu = Menu.buildFromTemplate([])
Menu.setApplicationMenu(menu)
// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) createWindow()
})

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', async () => {
  if (isDevelopment && !process.env.IS_TEST) {
    // Install Vue Devtools
    try {
      await installExtension(VUEJS3_DEVTOOLS)
    } catch (e) {
      console.error('Vue Devtools failed to install:', (e as object).toString())
    }
  }
  createWindow()
})

// Exit cleanly on request from parent process in development mode.
if (isDevelopment) {
  if (process.platform === 'win32') {
    process.on('message', (data) => {
      if (data === 'graceful-exit') {
        app.quit()
      }
    })
  } else {
    process.on('SIGTERM', () => {
      app.quit()
    })
  }
}
