/*
 * @Author: wuziyang 764401855@qq.com
 * @Date: 2022-06-29 15:40:06
 * @LastEditors: wuziyang 764401855@qq.com
 * @LastEditTime: 2022-06-29 16:24:12
 * @FilePath: \electron-vue-app\src\components\index.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
const modulesFiles = require.context("./rh-element-ui", true, /\.vue$/);

const modules = modulesFiles.keys().reduce((modules, modulePath) => {
  // set './app.js' => 'app'
  const moduleName = modulePath.replace(/^\.\/(.*)\.\w+$/, "$1");
  // 执行modulesFiles函数，返回一个对象{default: {// 文件内容}, _esModule: true}
  const value = modulesFiles(modulePath);
  modules[moduleName] = value.default;
  return modules;
}, {});

export default {
  install: (Vue) => {
    for(let key in modules) {
      Vue.component(key, modules[key])
    }
  }
}