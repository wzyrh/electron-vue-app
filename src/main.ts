/*
 * @Author: wuziyang 764401855@qq.com
 * @Date: 2022-06-22 09:37:11
 * @LastEditors: wuziyang 764401855@qq.com
 * @LastEditTime: 2022-07-12 09:29:38
 * @FilePath: \electron-vue-app\src\main.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import './premission.ts'
import store from "./store";
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import "@/assets/styles/index.scss"
import request from './utils/request'
import contextmenu from "v-contextmenu";
import "v-contextmenu/dist/themes/default.css";
var components = require('@/components').default
import $bus from '@/utils/bus'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
const app = createApp(App)
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}
app.config.globalProperties.$http = request
app.use(store).use(router).use(ElementPlus).use(components).use(contextmenu).mount("#app");
(window as any).$bus = $bus