/*
 * @Author: wuziyang 764401855@qq.com
 * @Date: 2022-06-22 09:37:11
 * @LastEditors: wuziyang 764401855@qq.com
 * @LastEditTime: 2022-07-11 14:32:25
 * @FilePath: \electron-vue-app\src\router\index.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import { createRouter, createWebHashHistory, RouteRecordRaw } from "vue-router";
import Layout from "@/layout/index.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "Home",
    redirect: '/home',
    component: Layout,
    children: [
      {
        path: "/home",
        name: "Home",
        component: () => import(/* webpackChunkName: "home" */ "../views/Home.vue"),
      },
      {
        path: "/myLog",
        name: "MyLog",
        component: () => import(/* webpackChunkName: "myLog" */ "../views/MyLog.vue"),
      }
    ]
  },
  {
    path: "/birthday",
    name: "Birthday",
    component: () => import(/* webpackChunkName: "birthday" */ "../views/birthday.vue"),
  },
  {
    path: "/login",
    name: "Login",
    component: () => import(/* webpackChunkName: "login" */ "../views/Login.vue"),
  },
  {
    path: '/redirect',
    component: Layout,
    children: [{
      path: '/redirect/:path*',
      component: () => import(/* webpackChunkName: "redirect" */ "../views/redirect/index.vue")
    }]
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});
router.beforeEach((to, form, next) => {
  if (!localStorage.token && !to.path.includes('login')) {
    next({ path: '/login' })
  } else {
    next()
  }
})
export default router;
