/*
 * @Author: wuziyang 764401855@qq.com
 * @Date: 2022-07-12 09:27:55
 * @LastEditors: wuziyang 764401855@qq.com
 * @LastEditTime: 2022-07-12 09:28:06
 * @FilePath: \electron-vue-app\src\utils\bus.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
class Bus {
	list: { [key: string]: Array<Function> };
	constructor() {
		// 收集订阅信息,调度中心
		this.list = {};
	}

	// 订阅
	$on(name: string, fn: Function) {
		this.list[name] = this.list[name] || [];
		this.list[name].push(fn);
	}

	// 发布
	$emit(name: string, data?: any) {
		if (this.list[name]) {
      		this.list[name].forEach((fn: Function) => {
        	fn(data);
      });
    }
	}

	// 取消订阅
	$off(name: string) {
		if (this.list[name]) {
			delete this.list[name];
		}
	}
}
export default new Bus();