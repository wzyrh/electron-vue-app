/*
 * @Author: wuziyang 764401855@qq.com
 * @Date: 2022-06-23 15:03:08
 * @LastEditors: rh 764401855@qq.com
 * @LastEditTime: 2022-07-19 16:06:09
 * @FilePath: \electron-vue-app\src\utils\request.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import axios from 'axios'
import { ElMessage } from 'element-plus'
import type { AxiosInstance, AxiosRequestConfig } from 'axios'
import store from '@/store'
import router from '@/router'

console.log(process.env.NODE_ENV === 'development' ? process.env.VUE_APP_BASE_API : location.origin)
// create an axios instance
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  withCredentials: false, // send cookies when cross-domain requests
  timeout: 10000, // request timeout
})
interface Config {
  headers?: any
  url?: any
}
// request interceptor
service.interceptors.request.use((config: AxiosRequestConfig) => {
  // do something before request is sent
  if (localStorage.token) {
    (config as Config).headers['Authorization'] = localStorage.token
  }
  if (config.method == 'get') {
    config.params = config.params ? config.params : config.data
  }
  const timestamp: any = new Date().getTime() / 1000
  if ((config as Config).url.indexOf('?') > -1) {
    config.url += `&n=${timestamp}`
  } else {
    config.url += `?n=${timestamp}`
  }
  return config
},
  error => {
    // do something with request error
    if (process.env.NODE_ENV === 'development') {
      console.log(error) // for debug
    }
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  response => {
    const res = response.data
    const config = response.config
    console.log(res)
    // let url = config.url
    
    if (res.code === 200) {
      ElMessage({
        message: res.msg || '操作成功',
        type: 'success',
        duration: 1500,
      })
      return res
    }
    if (res.code === 600 || res.code === 601 || res.code === 602) {
      if (window.location.pathname.indexOf('login') > -1) return
    } else {
      if (res.code === 403) {
        ElMessage({
          message: '登录时间太久啦，重新输入密码登录叭',
          type: 'error',
          duration: 1500,
        })
        localStorage.removeItem('token')
        router.replace('/login')
      } else if (res.code !== 200) {
        ElMessage({
          message: res.msg || '请求出错，请重试',
          type: 'error',
          duration: 1500,
        })
        return Promise.reject(new Error(res.msg || 'Error'))
      } else {
        return res
      }
    }
  },
  error => {
    if (process.env.NODE_ENV === 'development') {
      console.log(error) // for debug
    }
    ElMessage({
      message: '请求出错，请重试',
      type: 'error',
      duration: 1500
    })
    return Promise.reject(error)
  }
)
export default service
