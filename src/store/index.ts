/*
 * @Author: wuziyang 764401855@qq.com
 * @Date: 2022-06-22 09:37:11
 * @LastEditors: wuziyang 764401855@qq.com
 * @LastEditTime: 2022-07-06 14:19:13
 * @FilePath: \electron-vue-app\src\store\index.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import { createStore } from "vuex";
import createPersistedstate from 'vuex-persistedstate'
import user from './modules/user'
import menu from './modules/menu'
export default createStore({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    user,
    menu
  },
  plugins: [
    createPersistedstate({
      key: 'erabbit-client-pc-store',
      paths: ['user', 'menu']
    })
  ]
});
