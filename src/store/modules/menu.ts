/*
 * @Author: wuziyang 764401855@qq.com
 * @Date: 2022-06-23 16:51:04
 * @LastEditors: wuziyang 764401855@qq.com
 * @LastEditTime: 2022-07-06 14:28:16
 * @FilePath: \electron-vue-app\src\store\modules\user.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import request from '@/utils/request'
import router from '@/router'
interface State {
  activeIndex:String
}
export default {
  state: {
    activeIndex: '/home'
  },
  mutations: {
    SET_ACTIVE_INDEX(state:State, data: String) {
      state.activeIndex = data
    }
  },
  actions: {
    setActiveIndex({ commit }: any, data: String) {
      commit('SET_ACTIVE_INDEX', data)
    }
  },
  namespaced: true
}