/*
 * @Author: wuziyang 764401855@qq.com
 * @Date: 2022-06-23 16:51:04
 * @LastEditors: wuziyang 764401855@qq.com
 * @LastEditTime: 2022-07-06 14:47:25
 * @FilePath: \electron-vue-app\src\store\modules\user.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import request from '@/utils/request'
import router from '@/router'
export default {
  state: {
    userInfo: {},
    token: ''
  },
  mutations: {
    SET_USER_INFO(state:any, data:any) {
      state.userInfo = data
    },
    SET_TOKEN(state:any, data:string) {
      state.token = data
    }
  },
  actions: {
    login(store:any,data:any) {
      return new Promise(resolve => {
        request({
          url: '/users/login',
          method: 'POST',
          data
        }).then((res:any) => {
          store.commit('SET_USER_INFO', res.data.userInfo)
          store.commit('SET_TOKEN', res.data.token)
          store.commit('menu/SET_ACTIVE_INDEX', '/home', {root:true})
          localStorage.token = res.data.token
          router.push({ path: '/home' })
          resolve(res.data)
        })
      })
    },
    logout(store:any) {
      return new Promise(resolve => {
        store.commit('SET_USER_INFO', '')
        store.commit('SET_TOKEN', '')
        router.replace({ path: '/login' })
        store.commit('menu/SET_ACTIVE_INDEX', '/home', {root:true})
        resolve(true)
      })
    }
  },
  namespaced: true
}