/*
 * @Author: wuziyang 764401855@qq.com
 * @Date: 2022-06-22 14:56:37
 * @LastEditors: wuziyang 764401855@qq.com
 * @LastEditTime: 2022-06-29 15:07:35
 * @FilePath: \electron-vue-app\src\premission.ts
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
import router from './router'

router.beforeEach((to, form, next) => {
  const canvas:any = document.getElementById('canvas')
  canvas.style.display = to.path === '/login' ? 'block' : 'none'
  if (!localStorage.getItem('token') && to.path != '/login') {
    next({ path: 'login' })
  } else {
    next()
  }
})