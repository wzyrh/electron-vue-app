  var canvas = document.getElementById("canvas");
  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;
  var ctx = canvas.getContext("2d");
  var particles = []
  var count = parseInt(canvas.height/150*canvas.width/150);
  var maxDis = 150;
  var maxMouseDis = 250;
  var maxVelocity = 0.6;
  var mouseX = -1,mouseY = -1;
  var lastMouseX = -1,lastMouseY = -1;
  var mouseVelocity = 0;

  class Particle{
      constructor(x,y) {
          this.x = x;
          this.y = y;
          this.directionY = 0.5 - Math.random();
          this.directionX = 0.5 - Math.random();
      }
      update() {
          this.x += this.directionX;
          this.y += this.directionY;
      }
      draw() {
          ctx.beginPath();
          ctx.arc(this.x,this.y,2,0,Math.PI*2);
          ctx.fillStyle = "white";
          ctx.fill();
      }
  }

  function createParticle(){
      let x = Math.random() * canvas.width;
      let y = Math.random() * canvas.height;
      particles.push(new Particle(x, y));
  }

  function handleParticle(){
      particles.forEach((element,index) => {
          element.update();
          element.draw();
          if(element.x < 0 || element.x > canvas.width){
              element.directionX = - element.directionX;
          }
          if(element.y < 0 || element.y > canvas.height) {
              element.directionY = - element.directionY;
          }
          particles.forEach((aElement,index) => {
              distance = Math.sqrt( Math.pow(element.x - aElement.x,2) + Math.pow(element.y - aElement.y,2) );
              if(distance < maxDis) {
                  ctx.beginPath();
                  ctx.strokeStyle = "rgba(255,255,255," + (1 - distance / maxDis) + ")";
                  ctx.moveTo(element.x,element.y);
                  ctx.lineTo(aElement.x,aElement.y);
                  ctx.lineWidth = 1;
                  ctx.stroke();
              }
          })
      }); 
  }

  function handleMouse(){
      if(mouseX == -1 || mouseY == -1) return;
      particles.forEach((element,index) => {
          let distance = Math.sqrt( Math.pow(element.x - mouseX,2) + Math.pow(element.y - mouseY,2) );
          if(distance < maxMouseDis) {
              ctx.beginPath();
              ctx.strokeStyle = "rgba(255,255,255," + (1 - distance / maxMouseDis) + ")";
              ctx.moveTo(element.x,element.y);
              ctx.lineTo(mouseX,mouseY);
              ctx.lineWidth = 1;
              ctx.stroke();
              
              let velocity = Math.sqrt( Math.pow(element.directionX,2) + Math.pow(element.directionY,2) );
              if(distance > maxMouseDis / 3 * 2) {
                  element.directionX = - Math.min(Math.max(mouseVelocity,velocity),maxVelocity) * (element.x - mouseX) / distance * 1;
                  element.directionY = - Math.min(Math.max(mouseVelocity,velocity),maxVelocity) * (element.y - mouseY) / distance * 1;
              }
          }
      })
  }

  function draw(){
      ctx.clearRect(0,0,canvas.width,canvas.height);
      if(particles.length < count) {
          createParticle();
      }
      handleParticle();
      handleMouse();
  }

  setInterval(draw,10);

  canvas.addEventListener("mousemove",function(e) {
      mouseX = e.clientX;
      mouseY = e.clientY;
      if(lastMouseX == -1 || lastMouseY == -1) {
          mouseVelocity = 0;
      } else {
          mouseVelocity = Math.sqrt( Math.pow(lastMouseX - mouseX,2) + Math.pow(lastMouseY - mouseY,2) )
      }
      lastMouseX = mouseX;
      lastMouseY = mouseY;
  })

  canvas.addEventListener("mouseout",function(){
      mouseX = -1;
      mouseY = -1;
  })